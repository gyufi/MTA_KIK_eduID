<?php


'authproc' => array(
    // beállítjuk az affiliation-t
    40 => array(
        'class'                      => 'core:AttributeAdd',
        'eduPersonAffiliation' => array('member', 'library-walk-in'),

    ),

    // beállítjuk az scopedAffiliation-t
    60 => array(
        'class'                      => 'core:AttributeAdd',
        'eduPersonScopedAffiliation' => array(
            'member@idp.olvaso.mta.hu',
            'library-walk-in@idp.olvaso.mta.hu',
        ),

    ),

    // igen, adjunk ki TargetedID-t is
    61 => array(
        'class'  => 'core:TargetedID',
        'nameId' => true,
    ),

    // minden attribútum nevét átmappelünk oid formában, hogy a transfer rétegben már így menjenek.
    70 => array(
        'class' => 'core:AttributeMap',
        'name2oid'
    ),

    // kiszűrünk minden olyan attribútumot, amit nem kér tőlünk az SP.
    80 => 'core:AttributeLimit',
),