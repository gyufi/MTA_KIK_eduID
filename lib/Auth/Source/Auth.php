<?php

/**
 * Aleph authentication via Xservices.
 **
 * @author Szabó Gyula <gyufi@sztaki.hu>
 * @package SimpleSAMLphp
 */
class sspmod_authaleph_Auth_Source_Auth extends SimpleSAML_Auth_Source {

    /**
     * The key of the AuthId field in the state.
     */
    const AUTHID = 'sspmod_authaleph_Auth_Source_Auth.AuthId';

    /**
     * The string used to identify our states.
     */
    const STAGEID = 'sspmod_authaleph_Auth_Source_Auth.StageId';

    /** @var \GuzzleHttp\Client $guzzleClient */
    private $guzzleClient;

    //eduPersonPrincipalName, ezt generáljuk a usernévből (z308-key-data 02-es key), úgy, hogy konkatenáljuk a @olvaso.konyvtar.mta.hu sztringgel.
    //displayName, használnám a z303-name értékét
    //mail, használnám a z304-email-address értékét.
    private $attributeMap = array(
        //attributeName => {'xpath' => string, 'multi' => boolean}
        "displayName" => array(
            "xpath" => "//z303-name",
            "multi" => false,
        ),
        "mail"       => array(
            "xpath" => "//z304-email-address",
            "multi" => true,
        ),
    );

    private $scope;

    private $usernameContainerZ308KeyType;
    private $authorizedStatuses;

    public $helpUrl;
    public $registrationUrl;

    /**
     * sspmod_authaleph_Auth_Source_Auth constructor.
     *
     * @param array $info
     * @param array $config
     *
     * @throws Exception
     */
	public function __construct(array $info, array $config) {
		assert('is_array($info)');
		assert('is_array($config)');
		assert('is_string($config["scope"])');
		assert('is_string($config["usernameContainerZ308KeyType"])');
        assert('is_array($config["authorizedStatuses"])');
		assert('is_string($config["alephUrl"])');

		// Call the parent constructor first, as required by the interface
		parent::__construct($info, $config);

        $this->guzzleClient = new GuzzleHttp\Client(
            array(
                'base_uri' => $config['alephUrl'],
                'timeout'  => 2.0,
            )
        );
		$this->scope = $config['scope'];
	    $this->usernameContainerZ308KeyType = $config["usernameContainerZ308KeyType"];
	    $this->authorizedStatuses = $config['authorizedStatuses'];

        if (array_key_exists('helpUrl', $config)) {
            $this->helpUrl = $config['helpUrl'];
        }
        if (array_key_exists('registrationUrl', $config)) {
            $this->registrationUrl = $config['registrationUrlUrl'];
        }

	}

    /**
     * @param array $state
     */
    public function authenticate(&$state)
    {
        assert('is_array($state)');


        // We are going to need the authId in order to retrieve this authentication source later
        $state[self::AUTHID] = $this->authId;

        $id = SimpleSAML_Auth_State::saveState($state, self::STAGEID);

        $url = SimpleSAML_Module::getModuleURL('authaleph/login.php');
        \SimpleSAML\Utils\HTTP::redirectTrustedURL($url, array('AuthState' => $id));
    }

    /**
	 * Attempt to log in using the given username and password.
	 *
	 * On a successful login, this function should return the users attributes. On failure,
	 * it should throw an exception. If the error was caused by the user entering the wrong
	 * username or password, a SimpleSAML_Error_Error('WRONGUSERPASS') should be thrown.
	 *
	 * Note that both the username and the password are UTF-8 encoded.
	 *
	 * @param string $username  The username the user wrote.
	 * @param string $password  The password the user wrote.
	 * @return array  Associative array with the users attributes.
	 */
	protected function login($username, $password) {
		assert('is_string($username)');
		assert('is_string($password)');

		$borId = $username;
        if(!preg_match('/^[\w]+$/', $username)) {
            throw new SimpleSAML_Error_Exception('Formátum hiba a felhasználónévben.');
        }
        if(!preg_match('/^[\w]+$/', $password)) {
            throw new SimpleSAML_Error_Exception('Formátum hiba a jelszóban.');
        }
        $borrowerInfo = $this->getBorInfo($username, $password);
        $this->makeDecision($borId, $borrowerInfo);
        $attributes = $this->mapAttributes($borrowerInfo);

        $attributes['uid'] = array($username);
        $attributes['eduPersonPrincipalName'] = array($username .'@'. $this->scope);

        return $attributes;
	}

    public static function handleLogin($authState, $username, $password)
    {
        if (!isset($authState)) {
            throw new SimpleSAML_Error_BadRequest('Missing "State" parameter.');
        }
        $state = SimpleSAML_Auth_State::loadState($authState, self::STAGEID);
        $source = SimpleSAML_Auth_Source::getById($state[self::AUTHID]);
        if ($source === NULL) {
            throw new SimpleSAML_Error_Exception('Could not find authentication source with id ' . $state[self::AUTHID]);
        }
        if (! ($source instanceof self)) {
            throw new SimpleSAML_Error_Exception('Authentication source type changed.');
        }

        try {
            $attributes = $source->login($username, $password);
        } catch (\Exception $exception) {
            if ($exception instanceof SimpleSAML_Error_Exception) {
                return $exception->getMessage();
            } else {
                throw $exception;
            }
        }

        if (empty($attributes)) {
            throw new SimpleSAML_Error_Exception('User not authenticated after login page.');
        }

        /*
         * So, we have a valid user. Time to resume the authentication process where we
         * paused it in the authenticate()-function above.
         */

        $state['Attributes'] = $attributes;
        SimpleSAML_Logger::info("[AuthAleph] ".$username." user logged in.");
        SimpleSAML_Auth_Source::completeAuth($state);

        /*
         * The completeAuth-function never returns, so we never get this far.
         */
        assert('FALSE');
	}


    /**
     * Authenticate user to aleph and return the whole bor-info structure
     *
     * @param $bor_id
     * @param $verification
     * @return SimpleXMLElement
     *
     * @throws SimpleSAML_Error_Exception
     *
     */
    private function getBorInfo($bor_id, $verification)
    {
        $url = "X?op=bor-info&bor_id=".$bor_id."&verification=".$verification."&format=1";
        try {
            /* @var $borauthResponse */
            $borResponse = $this->guzzleClient->get($url);
        } catch (\GuzzleHttp\Exception\GuzzleException $exception) { // connection error
            throw new SimpleSAML_Error_Exception("A könyvtári azonosító alrendszer átmenetileg nem elérhető. (connection exception)");
        }

        $parsedBorRespone = new SimpleXMLElement($borResponse->getBody());
        if (!$parsedBorRespone) { // invalid XML
            throw new SimpleSAML_Error_Exception("A könyvtári azonosító alrendszer átmenetileg nem elérhető. (invalid xml)");
        }


        if ($parsedBorRespone->error) {
            $errorMessage = $parsedBorRespone->error->__toString();

            if ("Error retrieving Patron System Key" == $errorMessage) {    // invalid username or password
                throw new SimpleSAML_Error_Exception('Hibás felhasználónév vagy jelszó.');
            } else {
                throw new SimpleSAML_Error_Exception($errorMessage);
            }
        }

        return $parsedBorRespone;
	}


    /**
     * Make decision over the borrowed info
     *
     * @param string           $bor_id
     * @param SimpleXMLElement $borrowerInfo
     *
     * @throws SimpleSAML_Error_Exception()
     */
    private function makeDecision($bor_id, \SimpleXMLElement $borrowerInfo)
    {
        // usernameContainerZ308KeyType
        // /bor-info/z308/z308-key-type[.="usernameContainerZ308KeyType"]
        // /bor-info/z308/z308-key-data[.="$bor_id"]

        $accountAuthorized = false;
        $xpath = '//z308[z308-key-type="'.$this->usernameContainerZ308KeyType.'" and z308-key-data="'.$bor_id.'"]';
        $z308_nodes = $borrowerInfo->xpath($xpath);

        if (count($z308_nodes) != 1) {
            throw new SimpleSAML_Error_Exception("Hibás felhasználónév vagy jelszó.");
        }

        $accountAuthorized = false;
        foreach ($z308_nodes[0]->children() as $nodename => $node) {
            if ("z308-status" == $nodename && in_array($node, $this->authorizedStatuses)) {
                $accountAuthorized = true;
                break;
            }
        }

        if (! $accountAuthorized) {
            throw new SimpleSAML_Error_Exception("Az eduID azonosítás ennél a felhasználónál le van tiltva.");
        }

        // expiry
        // /bor-info/z305/z305-expiry-date
        $expiryArray = $borrowerInfo->xpath('//z305-expiry-date');
        $expiry = $expiryArray[0]->__toString();
        if ($expiry< date("Ymd")){
            throw new SimpleSAML_Error_Exception("Az azonosító lejárt. Lejárat dátuma: ".$expiry);
        }
    }

    /**
     * Map xpathes to attribute array
     *
     * @param \SimpleXMLElement $borrowerInfo borrower info xml
     *
     * @return array attributes
     */
    private function mapAttributes(\SimpleXMLElement $borrowerInfo)
    {
        $attributes = array();
        foreach ($this->attributeMap as $attributeName => $attributeProperty) {
            $elements = $borrowerInfo->xpath($attributeProperty['xpath']);

            if ($attributeProperty['multi']) {
                $resultArray = array();
                foreach ($elements as $element) {
                    $resultArray[] = $element->__toString();
                }
                $attributes[$attributeName] = $resultArray;
            } else {
                foreach ($elements as $element) {
                    $attributes[$attributeName] = array($element->__toString());   // egyértékű az attribútum, az utolsót választjuk authentikusnak.
                }
            }
        }

        return $attributes;
    }

    public static function renderForm()
    {

        /**
         * This page shows a username/password login form, and passes information from it
         * to the sspmod_core_Auth_UserPassBase class, which is a generic class for
         * username/password authentication.
         *
         * @author  Olav Morken, UNINETT AS.
         * @package SimpleSAMLphp
         */

        if (!array_key_exists('AuthState', $_REQUEST)) {
            throw new SimpleSAML_Error_BadRequest('Missing AuthState parameter.');
        }
        $authState = $_REQUEST['AuthState'];
        $errorMessage = "";
        if (array_key_exists('submit', $_REQUEST) and (!array_key_exists('username', $_REQUEST) or !array_key_exists('password', $_REQUEST))) {
            $errorMessage = 'Felhasználónevet és jelszót ki kell tölteni!';
        } elseif (array_key_exists('submit', $_REQUEST)) {
            $errorMessage = sspmod_authaleph_Auth_Source_Auth::handleLogin($authState, $_REQUEST['username'], $_REQUEST['password']);
        }

        $config = sspmod_authaleph_Auth_Source_Auth::getById('authaleph');

        $helpLink = array();
        $registrationLink = array();
        if ($config->helpUrl) {
            $helpLink = array("value" => "Segítség", "href" => $config->helpUrl);
        };

        if ($config->registrationUrl) {
            $registrationLink = array("value" => "Regisztráció", "href" => $config->registrationUrl);
        };

        $engine = \SimpleSAML\Modules\Twig\Facade\Twig::getInstance();

        echo $engine->render('@authaleph/login.html.twig',
            array(
                "authState"    => $authState,
                "submitButton" => "Belépés",
                "links"        => array(
                    "register" => $registrationLink,
                    "help"     => $helpLink,
                ),
                "placeholders" => array(
                    "username" => "felhasználónév",
                    "password" => "jelszó",
                ),
                "errorMessage" => $errorMessage,
            )
        );

        exit();
    }
}
