[![pipeline status](https://git.hbit.sztaki.hu/gyufi/MTA_KIK_eduID/badges/master/pipeline.svg)](https://git.hbit.sztaki.hu/gyufi/MTA_KIK_eduID/commits/master) [![coverage report](https://git.hbit.sztaki.hu/gyufi/MTA_KIK_eduID/badges/master/coverage.svg)](https://git.hbit.sztaki.hu/gyufi/MTA_KIK_eduID/commits/master)
# AuthAleph

This module provide Ex Libris Aleph to act Identity Provider role to SAML federations with power of simpleSAMLphp.

## Install

You can easy install the module to a simpleSAMLphp instance by composer:

`composer require szabogyula/simplesamlphp-module-authaleph`

## Usage

Edit the `config/authsources.php`

```php
$config = array(

    ...
    'authaleph' => array(
        'authaleph:Auth',

        // base url of the aleph X service, ex: http://aleph.net
        'alephUrl'                     => "http://aleph.net",

        // the scope of the IdP. The ePPN generate with this postfix.
        //  and the eduPersonScopedAffiliaton ex: aleph.net 
        // 
        'scope'                        => "aleph.net",

        // the Z308 key type of the account used for authentication, ex: 02
        'usernameContainerZ308KeyType' => "02",
        
        // Z308-status values to authorize to login. Has to be an array. Required.
        'authorizedStatuses' => ['ED'],

        // _optional_ help and registration url-s.
        // 'registrationUrl' => 'http://aleph.net/registration',
        // 'helpUrl' => 'http://aleph.net/help',
    ),
    ...
);
```
You should customize the login form by modify the `resources/templates/login.html.twig`.
It uses Twig template engine.

## Development

Look around in the [development](development) directory.
You will find ansible rolebook and Dockerfile that provide an IdP. Mounting the code into
the container you can coding easy.

**docker build and run**

`cd development; docker-composer up`

### Debts
* i18n
