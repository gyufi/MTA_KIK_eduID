<?php

/**
 * Created by PhpStorm.
 * User: gyufi
 * Date: 2017. 11. 30.
 * Time: 17:30
 * Test response service at: mockable.io
 *
 * @coversDefaultClass sspmod_authaleph_Auth_Source_Auth
 */

class sspmod_authaleph_Auth_Source_AuthTest extends PHPUnit_Framework_TestCase
{

    /**
     * set up test environmemt
     */
    public function setUp()
    {

        $config = array('key' => 'value');
        $authsources = array(
            'authaleph' => array(
                'authaleph:Auth',
                'alephUrl'                     => "http://demo9971524.mockable.io/",
                'scope'                        => "konyvtar.mta.hu",
                'usernameContainerZ308KeyType' => "02",
                'authorizedStatuses' => ['ED'],
                // optional help and registraiion url-s.
                //'registrationUrl' => 'http://aleph.net/registration',
                //'helpUrl' => 'http://aleph.net/help',
            ),
        );


        $configAsString = var_export($config, 1);
        $authsourcesAsString = var_export($authsources, 1);

        if (!file_exists(\SimpleSAML\Utils\Config::getConfigDir())) {
            mkdir(\SimpleSAML\Utils\Config::getConfigDir());
        }
        if (!file_exists(\SimpleSAML\Utils\Config::getConfigDir() . '/config.php')) {
            file_put_contents(\SimpleSAML\Utils\Config::getConfigDir() . '/config.php', '<?php $config = ' . $configAsString . ';');
        }
        if (!file_exists(\SimpleSAML\Utils\Config::getConfigDir() . '/authsources.php')) {
            file_put_contents(\SimpleSAML\Utils\Config::getConfigDir() . '/authsources.php', '<?php $config = ' . $authsourcesAsString . ';');
        }
    }

    private $info = array(
        "AuthId" => "valamiauthid",
    );

    private $config = array(
        "scope"                        => "konyvtar.mta.hu",
        "usernameContainerZ308KeyType" => "02",
        "alephUrl"                     => "http://demo9971524.mockable.io/",
        'authorizedStatuses'           => ['ED'],
    );

    private $expectedAttributes = array(
        'displayName'            =>
            array(
                0 => 'Marmonk Anna',
            ),
        'mail'                   =>
            array(
                0 => 'marmonk.anna@konyvtar.mta.hu',
            ),
        'uid'                    =>
            array(
                0 => 'user',
            ),
        'eduPersonPrincipalName' =>
            array(
                0 => 'user@konyvtar.mta.hu',
            ),
    );

    private $expectedMapAttributes = array(
        'displayName' =>
            array(
                0 => 'Marmonk Anna',
            ),
        'mail'        =>
            array(
                0 => 'marmonk.anna@konyvtar.mta.hu',
            ),
    );

    private $username = "user";
    private $password = "pass";
    private $unauthorizedUsername = "unauthuser";


    /**
     * @covers ::__construct
     */
    public function testConstructor()
    {

        $auth = new sspmod_authaleph_Auth_Source_Auth($this->info, $this->config);

        $this->assertInstanceOf("sspmod_authaleph_Auth_Source_Auth", $auth);
    }


    /**
     * @covers ::getBorInfo
     * @covers ::makeDecision
     * @covers ::mapAttributes
     */
    public function testGetBorInfo()
    {

        $auth = new sspmod_authaleph_Auth_Source_Auth($this->info, $this->config);

        $borInfo =  $this->invokeMethod($auth, 'getBorInfo', array($this->username, $this->password));

        $this->assertInstanceOf(SimpleXMLElement::class, $borInfo);

        $this->invokeMethod($auth, 'makeDecision', array($this->username, $borInfo));
        $attributes = $this->invokeMethod($auth, 'mapAttributes', array($borInfo));
        $this->assertEquals($this->expectedMapAttributes, $attributes);
    }

    /**
     * @covers ::login
     */
    public function testLogin()
    {
        $auth = new sspmod_authaleph_Auth_Source_Auth($this->info, $this->config);
        $attributes =  $this->invokeMethod($auth, 'login', array($this->username, $this->password));
        $this->assertEquals($this->expectedAttributes, $attributes);
    }

    /**
     * @covers ::makeDecision
     * @expectedException SimpleSAML_Error_Exception
     * @expectedExceptionMessage Az azonosító lejárt. Lejárat dátuma: 20170614
     */
    public function testExpiredAccount()
    {
        $user = "expired_user";
        $auth = new sspmod_authaleph_Auth_Source_Auth($this->info, $this->config);
        $borInfo = $this->invokeMethod($auth, 'getBorInfo', array($user, $this->password));
        $this->invokeMethod($auth, 'makeDecision', array($user, $borInfo));
    }

    /**
     * @covers ::getBorInfo
     * @expectedException SimpleSAML_Error_Exception
     * @expectedExceptionMessage Hibás felhasználónév vagy jelszó.
     */
    public function testInvalidUsername()
    {
        $auth = new sspmod_authaleph_Auth_Source_Auth($this->info, $this->config);
        $this->invokeMethod($auth, 'getBorInfo', array("invalid_username", $this->password));

    }

    /**
     * @covers ::makeDecision
     * @expectedException SimpleSAML_Error_Exception
     * @expectedExceptionMessage Hibás felhasználónév vagy jelszó.
     */
    public function testUnmatchedZ308KeyType()
    {
        $user = "bad_key";
        $auth = new sspmod_authaleph_Auth_Source_Auth($this->info, $this->config);
        $borInfo = $this->invokeMethod($auth, 'getBorInfo', array($user, "bad_key"));
        $this->invokeMethod($auth, 'makeDecision', array($user, $borInfo));
    }

    /**
     * @covers ::getBorInfo
     * @expectedException SimpleSAML_Error_Exception
     * @expectedExceptionMessage Hibás felhasználónév vagy jelszó.
     */
    public function testInvalidPassword()
    {
        $auth = new sspmod_authaleph_Auth_Source_Auth($this->info, $this->config);
        $this->invokeMethod($auth, 'getBorInfo', array($this->username, "invalid_password"));
    }

    /**
     * @covers ::makeDecision
     * @expectedException SimpleSAML_Error_Exception
     * @expectedExceptionMessage Az eduID azonosítás ennél a felhasználónál le van tiltva.
     */
    public function testUnauthorizedUser()
    {
        $auth = new sspmod_authaleph_Auth_Source_Auth($this->info, $this->config);
        $borInfo = $this->invokeMethod($auth, 'getBorInfo', array($this->unauthorizedUsername, $this->password));
        $this->invokeMethod($auth, 'makeDecision', array($this->unauthorizedUsername, $borInfo));
    }

    /**
     * @covers ::renderForm
     * @expectedException Twig_Error_Loader
     * @expectedExceptionMessage There are no registered paths for namespace "authaleph".
     */
    public function testRenderForm()
    {
        $_REQUEST['AuthState'] = "macska";
        sspmod_authaleph_Auth_Source_Auth::renderForm();
    }

    /**
     * Call protected/private method of a class.
     *
     * @param object &$object Instantiated object that we will run method on.
     * @param string $methodName Method name to call
     * @param array $parameters Array of parameters to pass into method.
     *
     * @return mixed Method return.
     *
     * @throws ReflectionException
     */
    public function invokeMethod(&$object, $methodName, array $parameters = array())
    {
        $reflection = new \ReflectionClass(get_class($object));
        $method = $reflection->getMethod($methodName);
        $method->setAccessible(true);

        return $method->invokeArgs($object, $parameters);
    }
}